cmake_minimum_required(VERSION 3.3)
project(voxl-vision-hub)

set(CMAKE_C_FLAGS "-O2 -std=gnu99 -Wall -Wextra -Wuninitialized \
	-Wunused-variable -Wdouble-promotion -Wmissing-prototypes \
	-Wmissing-declarations -Werror=undef -Wno-unused-function ${CMAKE_C_FLAGS}")

# tell the linker not to worry about missing symbols in libraries
set(CMAKE_C_FLAGS   "-Wl,--unresolved-symbols=ignore-in-shared-libs ${CMAKE_C_FLAGS}")
set(CMAKE_CXX_FLAGS "-std=c++11 -O3 -Wl,--unresolved-symbols=ignore-in-shared-libs ${CMAKE_CXX_FLAGS}")


set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

option(PLATFORM "platform APQ8096 or QRB5165" ERROR)

if(PLATFORM MATCHES APQ8096)
	message(STATUS "Building for platform APQ8096")
	add_definitions(-DPLATFORM_APQ8096 )
elseif(PLATFORM MATCHES QRB5165)
	message(STATUS "Building for platform QRB5165")
	add_definitions(-DPLATFORM_QRB5165 )
else()
	message(FATAL_ERROR "Platform type not specified")
endif()

add_subdirectory (src)
add_subdirectory (utils)
add_subdirectory (voxl_trajectory)
